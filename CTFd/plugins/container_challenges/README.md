
# Container Challenges

Welcome, this documentation file will explain the container challenges plugin both briefly and thoroughly. The container challenges plugin is a plugin that supports the creation and execution of container challenges within CTFd. This allows for more complex challenges than the default CTFd challenges. Multiple technologies are used to make this plugin work, as listed in the `Used Technologies/Concepts` section.

## 1. Features

- Gitlab registry configuration
- Uploading Docker challenges
- Converting Docker challenges
- Deploying Docker challenges in Kubernetes
- Destroying Docker challenges in Kubernetes
- Kubernetes namespace management

## 2. Happy Flow

> - Plugin initializes itself and registers container challenge option
> - Admin uploads Gitlab registry username to CTFd
> - Admin uploads Gitlab registry password to CTFd
> - Plugin stores Gitlab registry username in database
> - Plugin stores Gitlab registry password in database
> - Admin uploads docker-compose file to CTFd
> - Admin creates container challenge
> - CTFd stores new challenge in database
> - User starts container challenge
> - Plugin deploys challenge container in Kubernetes
> - User stops container challenge
> - Plugin undeploys challenge container in Kubernetes

## 3. Technical Documentation

- [Gitlab Registry](readme/GITLAB_REGISTRY.md)
- [Configuration](readme/CONFIGURATION.md)
- [Plugin Structure](readme/PLUGIN_STRUCTURE.md)

## 4. Abbreviations

- JCR = Joint Cyber Range
- CTF = Capture The Flag
- JS = JavaScript
- HTML = HyperText Markup Language
- CSS = Cascading Style Sheets

## 5. Used Technologies/Concepts

- [CTFd](https://ctfd.io/)
- [Python](https://en.wikipedia.org/wiki/Python_(programming_language))
- [Flask](https://en.wikipedia.org/wiki/Flask_(web_framework))
- [Docker](https://en.wikipedia.org/wiki/Docker_(software))
- [Kubernetes](https://en.wikipedia.org/wiki/Kubernetes)
- [HTML](https://en.wikipedia.org/wiki/HTML)
- [CSS](https://en.wikipedia.org/wiki/CSS)
- [JavaScript](https://en.wikipedia.org/wiki/JavaScript)
- [AJAX](https://en.wikipedia.org/wiki/Ajax_(programming))
- [JQuery](https://en.wikipedia.org/wiki/JQuery)
- [Base64](https://en.wikipedia.org/wiki/Base64)
- [Happy Flow](https://en.wikipedia.org/wiki/Happy_path)
- [Gitlab](https://en.wikipedia.org/wiki/GitLab)
- [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration)
- [YAML](https://en.wikipedia.org/wiki/YAML)
- [Custom JCR deployment image](https://gitlab.com/jointcyberrange.nl/container-challenge-image)
