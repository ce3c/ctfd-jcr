# SURFconext

SURFconext is a Dutch federated identity management service, used by a lot of Dutch organizations (primarily scientific institutions). SURFconext also supports SSO. The JCR uses the accounts of the IDPs that SURFconext supports (currently HU and THUAS). This means that students can login through SURFconext with their institution account. It is important to understand that SURFconext is completely outside of the JCR systems. They handle the logging in with IDP accounts and this plugin simply receives the user data if the login attempt is successful. Currently, there are 2 types of SURFconext environments (also known as "entities") defined in the [SURFconext dashboard](https://sp.surfconext.nl/).

## 1. SURFconext Entities

<img src="surf_dashboard.png" width="50%"/>

* Protocol (we are using OIDC)
* Client ID (changeable in dashboard)
* Client secret (generated in dashboard only once)
* HTTPS configuration (important requirement)
* Redirect URI (after the IDP login was successful)

## 2. Connection Credentials

To allow the plugin to connect to the SURFconext entities, 3 variables are defined in the [CTFd configuration](../../config.ini) that can be retrieved by the plugin's Python code.

```ini
[oauth]
SURF_META =
SURF_SCHEME =
SURF_CLIENT_ID =
SURF_CLIENT_SECRET =
```

As you can see, these variables are empty, which means they are only defined, not filled in. Their value is stored in the system's ENV variables under the same name. See [this documentation](ENV_VARIABLES.md) for more detailed information about ENV variabels.

## 3. HTTPS

### 3.1 Code

```python
@custom_auth.route("/auth/surfconext/authorize", methods=["GET"])
def surfconext_authorize():
    return oauth.surfconext.authorize_redirect(url_for("custom_auth.surfconext_confirm", _external=True, _scheme=get_app_config("SURF_SCHEME")))
```

An important requirement for this plugin is that the connection with SURFconext happens through HTTPS. Before the user is sent to SURFconext, the scheme is retrieved from the `SURF_SCHEME` ENV variable.

### 3.2 Scheme Error

During the redirection process within the plugin, an HTTP 400 error can occur that is very unpleasant to read. The message contains a lot of weird characters (example: `µ®Rê îKz&5 úúÀ+À/À,À0Ì©Ì¨À`). This means that the `SURF_SCHEME` ENV variable is not correct.

## 4. Redirect URI

### 4.1 Code

```python
@custom_auth.route("/auth/surfconext/confirm", methods=["GET"])
def surfconext_confirm():
    # code is omitted
```

After a successful IDP login, SURFconext will redirect the user back to the JCR website. At least 1 redirect URI **must** be defined in the dashboard. This plugin uses the path `/auth/surfconext/confirm`. Example URI: `https://hu.jointcyberrange.nl/auth/surfconext/confirm`.

### 4.2 URI Error

During the redirection process within SURFconext, an HTTP 400 error (`invalid_request_uri`) can occur. This means that the website that sent the user to SURFconext is not registered as redirect URI in the SURFconext dashboard. Take note that SURFconext is very strict about these URIs. For example: `127.0.0.1` is NOT the same as `localhost`. 

## 5. Sequence Diagram

<img src="surf_diagram.png" width="100%"/>

**NOTE**: for more detailed information about the SURFconext login process, visit [this article](https://communities.surf.nl/artikel/surfconext-openid-connect-voor-dummies)